#!/usr/bin/env python
 
# This simple example shows how to do basic rendering and pipeline
# creation.
 
import vtk
# The colors module defines various useful colors.
from vtk.util.colors import tomato

from PyQt5.QtWidgets import (QApplication)
from PyQt5 import Qt
import sys
import vtk
from vtk.qt.QVTKRenderWindowInteractor import QVTKRenderWindowInteractor
from scipy import misc
import matplotlib.pyplot as plt
from vtk.util import numpy_support
import numpy as np
from os import listdir
from os.path import join

import PyQt5.QtCore
import matplotlib
import sys

class ImageUtil:

	# The image is read using the scikit imgproc's imread that returns the image in column major format.
	# A simple vertical line is added to the image.
	# Returns a 3D array a[w,h,d] where the depth encodes the stack element
	@staticmethod
	def read_volume_disk(stack_path):
		files = listdir(stack_path)

		test = misc.imread(join(stack_path, files[0]))
		print('test.spahe: {}'.format(test.shape))
		result = np.ndarray(shape=(test.shape[0], test.shape[1], len(files)-1), dtype=np.uint8)

		for i in range(1, len(files)):
			stack_element_path = join(stack_path, str(i) + '.tif')
			stack_element = misc.imread(stack_element_path)

			result[:, :, i-1] = stack_element[:, :, 1]

		return result

	@staticmethod
	def convert_image_vtk(frame):
		im = vtk.vtkImageData()
		vtk_arr = numpy_support.numpy_to_vtk(frame.ravel(order='C'), deep=True, array_type=vtk.VTK_UNSIGNED_CHAR)
		im.GetPointData().SetScalars(vtk_arr)
		im.SetDimensions(frame.shape[0], frame.shape[1], 1)
		return im

	@staticmethod
	def convert_volume_vtk(depth_frame, rgbFrame=None):
		# Build vtkImageData here from the given numpy uint8_t arrays.
		im = vtk.vtkImageData()
		depth_array = numpy_support.numpy_to_vtk(depth_frame.transpose(1, 0, 2).ravel(order='F'), deep=True, array_type=vtk.VTK_UNSIGNED_CHAR)

		# Transpose(2, 0, 1) may be required depending on numpy array order see -
		# https://github.com/quentan/Test_ImageData/blob/master/TestImageData.py

		im.SetDimensions(depth_frame.shape)
		# assume 0,0 origin and 1,1 spacing.
		im.SetSpacing([1, 1, 1])
		im.SetOrigin([0, 0, 0])
		im.GetPointData().SetScalars(depth_array)

		return im


class ModelViewport:
	def __init__(self, vtk_widget, volume, stack_pos):
		self.vtk_widget = vtk_widget
		self.volume = volume
		self.stack_pos = stack_pos

	def refresh(self):
		self.vtk_widget.GetRenderWindow().GetInteractor().Render()

	@staticmethod
	def bind_renderer_to_widget(renderer, widget):
		widget.GetRenderWindow().AddRenderer(renderer)
		iren = widget.GetRenderWindow().GetInteractor()
		iren.Initialize()


class VolumeViewport(ModelViewport):
	def __init__(self, vtk_widget, volume, stack_pos):
		ModelViewport.__init__(self, vtk_widget, volume, stack_pos)

		self.intersection_plane_xy = self.create_plane(*self.volume.shape[0:2])                     #moves in z
		self.intersection_plane_yz = self.create_plane(*self.volume.shape[1:3])                     #moves in x
		self.intersection_plane_xz = self.create_plane(self.volume.shape[0], self.volume.shape[2])  #moves in y

		self.volume = self.create_volume()
		self.volume_renderer = self.init_renderer()

	@staticmethod
	def create_plane(w, h):
		canvas_source = vtk.vtkImageCanvasSource2D()
		canvas_source.SetExtent(0, w, 0, h, 0, 0)
		canvas_source.SetScalarTypeToUnsignedChar()
		canvas_source.SetNumberOfScalarComponents(3)
		canvas_source.SetDrawColor(127, 0, 127)
		canvas_source.FillBox(0, w, 0, h)
		canvas_source.SetDrawColor(100, 255, 255)
		canvas_source.FillTriangle(10, 10, 25, 10, 25, 25)
		canvas_source.SetDrawColor(255, 100, 255)
		canvas_source.FillTube(75, 75, 0, 75, 5.0)
		canvas_source.Update()

		#image_resize = vtk.vtkImageResize()
		#image_resize.SetInputData(canvas_source.GetOutput())
		#image_resize.SetOutputDimensions(50, 50, 1)
		#image_resize.Update()

		image_actor = vtk.vtkImageActor()
		image_actor.GetMapper().SetInputConnection(canvas_source.GetOutputPort())

		return image_actor

	def rerender_plane(self):
		#self.volume_renderer.RemoveActor(self.intersection_plane_actor)
		#self.intersection_plane_actor = self.create_outline(512, 512)
		#self.volume_renderer.AddActor(self.intersection_plane_actor)
		f_stack_pos = [float(e) for e in self.stack_pos]
		self.intersection_plane_xy.SetPosition(0, 0, f_stack_pos[2])
		self.intersection_plane_xz.SetPosition(0, f_stack_pos[1], 0)
		self.intersection_plane_yz.SetPosition(f_stack_pos[0], 0, 0)
		self.refresh()

	def create_volume(self):
		vtk_image = ImageUtil.convert_volume_vtk(self.volume)

		# Flip the image around the Y axis.

		flip_filter = vtk.vtkImageFlip()
		flip_filter.SetFilteredAxis(1)
		flip_filter.SetInputData(vtk_image)
		flip_filter.Update()

		volume_mapper = vtk.vtkSmartVolumeMapper()
		#volume_mapper = vtk.vtkFixedPointVolumeRayCastMapper()
		volume_mapper.SetBlendModeToComposite()
		volume_mapper.SetBlendModeToComposite()
		volume_mapper.SetInputConnection(flip_filter.GetOutputPort())

		thresh = 120

		color_transform = vtk.vtkColorTransferFunction()
		color_transform.AddRGBPoint(0, 0.3, 0.3, 0.3)
		color_transform.AddRGBPoint(50, 0.0, 0.1, 0.0)
		color_transform.AddRGBPoint(thresh, 0.7, 0.7, 0.1)

		opacity = vtk.vtkPiecewiseFunction()
		opacity.AddPoint(0, 0.0)
		opacity.AddPoint(thresh, 0.9)

		volume_properties = vtk.vtkVolumeProperty()
		volume_properties.SetColor(color_transform)
		volume_properties.SetScalarOpacity(opacity)

		volume_properties.ShadeOn()
		volume_properties.SetSpecular(1.0)
		volume_properties.SetAmbient(1.0)
		volume_properties.SetDiffuse(1.0)

		volume_properties.SetInterpolationType(vtk.VTK_LINEAR_INTERPOLATION)

		volume = vtk.vtkVolume()
		volume.SetProperty(volume_properties)
		volume.SetMapper(volume_mapper)

		return volume

	def init_renderer(self):
		# Add the actors to the renderer, set the background and size

		volume_renderer = vtk.vtkRenderer()

		#volume_renderer.AddActor(self.cube_actor)
		volume_renderer.SetBackground(0.3, 0.3, 0.4)
		volume_renderer.AddActor(self.intersection_plane_xy)
		volume_renderer.AddActor(self.intersection_plane_yz)
		volume_renderer.AddActor(self.intersection_plane_xz)
		#self.intersection_plane_actor.GetProperty().SetOpacity(0.2)
		volume_renderer.AddVolume(self.volume)

		# We'll zoom in a little by accessing the camera and invoking a "Zoom"
		# method on it.
		volume_renderer.ResetCamera()
		cam = volume_renderer.GetActiveCamera()
		cam.Zoom(1.0)
		print('Camera view up vector: {}'.format(cam.GetViewUp()))
		l = [1, 1, 1]
		cam.GetEyePosition(l)
		print('Camera eye position: {}'.format(l))

		# ... End of VTK code ...

		ModelViewport.bind_renderer_to_widget(volume_renderer, self.vtk_widget)
		return volume_renderer

	def update_stack_pos(self, stack_pos):
		print('Updating stack pos to: {}'.format(stack_pos))
		self.stack_pos = stack_pos
		self.rerender_plane()


class ThreeViewViewport(ModelViewport):
	def __init__(self, vtk_widget, volume, stack_pos, axis_id):
		ModelViewport.__init__(self, vtk_widget, volume, stack_pos)
		self.axis_id = axis_id

		# Add the objects: image view, intersection plane, etc.
		self.slice_actor = vtk.vtkImageActor()
		self.imRenderer = vtk.vtkRenderer()
		self.stack_navigation_observers = []
		self.init_three_view()

	def prepare_slice_actor(self):
		v = np.take(self.volume, self.stack_pos[self.axis_id], self.axis_id)
		d = ImageUtil.convert_image_vtk(v)

		flip_filter = vtk.vtkImageFlip()
		flip_filter.SetFilteredAxis(1)
		flip_filter.SetInputData(d)
		flip_filter.Update()

		slice_actor = vtk.vtkImageActor()
		#slice_actor.GetMapper().SetInputConnection(flip_filter.GetOutputPort())
		slice_actor.SetInputData(d)

		return slice_actor

	def rerender_slice(self):
		self.slice_actor = vtk.vtkImageActor()
		self.imRenderer.RemoveActor(self.slice_actor)
		self.slice_actor = self.prepare_slice_actor()
		self.imRenderer.AddActor(self.slice_actor)
		self.imRenderer.GetRenderWindow().GetInteractor().Render()

	def init_three_view(self):
		self.slice_actor = self.prepare_slice_actor()
		self.imRenderer.AddActor(self.slice_actor)

		iStyle = vtk.vtkInteractorStyle()

		iStyle.AddObserver(vtk.vtkCommand.MouseWheelForwardEvent, self.mouse_wheel_fw, 1)
		iStyle.AddObserver(vtk.vtkCommand.MouseWheelBackwardEvent, self.mouse_wheel_bw, 2)

		self.vtk_widget.GetRenderWindow().AddRenderer(self.imRenderer)
		self.imRenderer.SetBackground(0.3, 0.3, 0.4)
		iren = self.vtk_widget.GetRenderWindow().GetInteractor()
		iren.SetInteractorStyle(iStyle)
		iren.Initialize()

	def mouse_wheel_fw(self, n, k):
		if self.stack_pos[self.axis_id] < self.volume.shape[self.axis_id] - 1:
			self.stack_pos[self.axis_id] += 1
		self.rerender_slice()
		self.notify_stack_navigation_obsersers(self.stack_pos)

	def mouse_wheel_bw(self, n, k):
		if self.stack_pos[self.axis_id] > 0:
			self.stack_pos[self.axis_id] -= 1
		self.rerender_slice()
		self.notify_stack_navigation_obsersers(self.stack_pos)

	def add_stack_navigation_observer(self, observer):
		self.stack_navigation_observers.append(observer)

	def notify_stack_navigation_obsersers(self, stack_pos):
		for observer in self.stack_navigation_observers:
			observer.notify(stack_pos)


class StackPosUpdateObserver:
	def __init__(self, volume_viewer):
		self.volume_viewer = volume_viewer

	def notify(self, new_stack_pos):
		self.volume_viewer.update_stack_pos(new_stack_pos)


class MainView(Qt.QMainWindow):
	def __init__(self, parent=None):
		Qt.QMainWindow.__init__(self, parent)
		self.setWindowTitle('BIOMAG Analyser')

		# A frame for the volume
		frame_3d, widget_3d = self.create_viewport_frame()

		# The three views
		frame_xy, widget_xy = self.create_viewport_frame()
		frame_yz, widget_yz = self.create_viewport_frame()
		frame_xz, widget_xz = self.create_viewport_frame()

		# Create the splitters and add the views

		###########
		# 3D # XY #
		###########
		# XY # YZ #
		###########

		horizontal_splitter_top = Qt.QSplitter(PyQt5.QtCore.Qt.Horizontal)
		horizontal_splitter_top.setStyleSheet("QSplitter::handle:horizontal {width: 10px;}")
		horizontal_splitter_top.addWidget(frame_3d)
		horizontal_splitter_top.addWidget(frame_xy)

		horizontal_splitter_bottom = Qt.QSplitter(PyQt5.QtCore.Qt.Horizontal)
		horizontal_splitter_bottom.addWidget(frame_xz)
		horizontal_splitter_bottom.addWidget(frame_yz)

		vertical_splitter = Qt.QSplitter(PyQt5.QtCore.Qt.Vertical)
		vertical_splitter.setStyleSheet("QSplitter::handle:vertical {height: 1px;}")
		vertical_splitter.addWidget(horizontal_splitter_top)
		vertical_splitter.addWidget(horizontal_splitter_bottom)

		self.setCentralWidget(vertical_splitter)

		# Process the input image

		image_path = '/home/etasnadi/Documents/phd-research-seminar/imgs/raw-image-yeast'

		self.volume = ImageUtil.read_volume_disk(image_path)

		self.stackPos = [0, 0, 0]

		# Create the volume viewer and the different views and set up them to render
		# their results to the corresponding widgets.

		vp_3d = VolumeViewport(widget_3d, self.volume, self.stackPos)
		stack_pos_observer = StackPosUpdateObserver(vp_3d)

		# Render the volume in different views. The axis is defined in the image coordinate system.
		vp_xy = ThreeViewViewport(widget_xy, self.volume, self.stackPos, 2)
		vp_xy.add_stack_navigation_observer(stack_pos_observer)

		vp_yz = ThreeViewViewport(widget_yz, self.volume, self.stackPos, 0)
		vp_yz.add_stack_navigation_observer(stack_pos_observer)

		vp_xz = ThreeViewViewport(widget_xz, self.volume, self.stackPos, 1)
		vp_xz.add_stack_navigation_observer(stack_pos_observer)

		# Start the event loop.
		self.show()

	@staticmethod
	def create_viewport_frame():
		frame = Qt.QFrame()
		layout = Qt.QVBoxLayout()
		widget = QVTKRenderWindowInteractor(frame)
		layout.addWidget(widget)
		frame.setLayout(layout)
		return frame, widget


def main():
	app = QApplication(sys.argv)
	ex = MainView()
	ex.show()
	sys.exit(app.exec())

main()
